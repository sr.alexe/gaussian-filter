import os
import easygui
import cv2
import numpy as np
import struct
from intelhex import hex2bin
import time
import glob
from win32api import GetSystemMetrics


SUPPORTED_FILE_TYPES = [["*.png", "*.jpeg", "*.jpg",
                         "*.jpe", "*.bmp", "*.dib", "Image Files"]] ##Extenciones de las imagenes que pueden ser utilizadas
DATA_PATH = "../data/" ## Constantes para la definicion de las rutas
ASSEMBLY_PATH = "../Assembly"
KEIL_PROJECT_PATH = "../Assembly/Image-sharpening.uvproj"
TEMP_PATH = DATA_PATH+"temporal/"
RESULT_PATH = DATA_PATH+"result/"
INPUT_PATH = DATA_PATH + "input/"
ABSOLUTE_PATH = os.path.dirname(os.path.abspath(__file__)) ## Se busca la ruta absulota del archivo .py desde donde se tomatrán las demas rutas
file_names = ["red", "green", "blue", "kernel1", "kernel2"] ## Array de objetos a binarizar
filter_names = ["red", "green", "blue"]
kernel1 = [[0, -1, 0], [-1, 5,-1], [0, -1, 0]] #Kernel de Sharpening
kernel2 = [[0, 0, 0], [0, 9, -4], [0, -4, 0]] #Kernel de Oversharpening
os.chdir(ABSOLUTE_PATH) ## Se cambia la ruta de ejecucion
folders = ["../data", "../data/temporal", "../data/result", "../data/input"]
images_k1 = [] # Array de imagenes resultantes
images_k2 = []
keil_folder ="C:\Keil_v5"
keil_exe_path=""


def AddColumnsAndRows(color_channel): #Funcion para agregar una columna y una final a la matriz de la imagen
    channel_height, channel_width = color_channel.shape
    resized_channel = np.zeros((channel_height+2, channel_width+2))
    resized_channel[1:-1, 1:-1] = color_channel
    resized_channel = resized_channel.astype(color_channel.dtype)
    return resized_channel

# Se crean las carpetas si no existen
for folder in folders: 
    if not os.path.exists(folder):
        os.mkdir(folder)

##Se solicita el folder de instalacion de Keil uVision si no es encontrado
if (not(os.path.isdir(keil_folder)) ):            
    keil_folder = easygui.diropenbox(
        default=keil_folder, msg="Seleccione la carpeta de instalacion de Keil uVision 5")

#Busca el ejecutable de Keil
for r, d, f in os.walk(keil_folder):
    for file in f:
        if 'UV4.exe' in file:
            keil_path = os.path.join(r, file)
            break

#Selecciona la imagen que se desea trabajar
image = cv2.imread(easygui.fileopenbox(msg="Elija la imagen a procesar",
                                       default="../data/*.png", filetypes=SUPPORTED_FILE_TYPES))
#Ingrese el alto y ancho de la imagen
try:
    height, width = easygui.multenterbox(msg='Fill in values for the fields.', title=' ', fields=("Alto","Ancho"), values=())
except :
    print("Shape will be calculated automatically")

# Se separan la imagen en capas RGB
blue_channel, green_channel, red_channel = cv2.split(image)
#se le agrega la columna y fila a las capas creadas
blue_channel = AddColumnsAndRows(blue_channel)
green_channel = AddColumnsAndRows(green_channel)
red_channel = AddColumnsAndRows(red_channel)
files = [red_channel, green_channel, blue_channel, kernel1, kernel2]


#Se calcula las nuevas dimensiones de la matriz
image_height, image_width = blue_channel.shape
image_byte_count = image_height*image_width*2

# Se inicializan el archivos binario con las dimensiones
dimensions_bin = open(DATA_PATH+'input/'+'dimensions.bin', 'wb')
dimensions_bin.write(struct.pack('<h', image_width))
dimensions_bin.write(struct.pack('<h', image_height))
dimensions_bin.flush()
dimensions_bin.close()

#Se crean los archivos binarios con las imagenes y los kernel
for (_file, name) in zip(files, file_names):
    temp_file = open(DATA_PATH+"input/"+name+'.bin', 'wb')
    for column in _file:
        for num in column:
            bin_number = struct.pack('<h', num)
            temp_file.write(bin_number)
            temp_file.flush()
    temp_file.close()

fin = open("../Assembly/commands_example.ini", "rt")
# output file to write the result to
fout = open("../Assembly/commands.ini", "wt")
# for each line in the input file
for line in fin:
    # read replace the string and write to output file
    fout.write(line.replace('HHHHHHH', str(hex(image_byte_count+0x1100000))))
# close input and output files
fin.close()
fout.close()

easygui.msgbox('Debido a la emulación de Hardware y el tamaño de la imagen, puede tardar hasta 5 minutos', 'Información')

#Imprime la direccion del proyecto en ensamblador
print(keil_path+" -b "+KEIL_PROJECT_PATH+" -j0")
#Compila y ejecuta el proyecto de Keil uVision en modo silencioso
os.system(keil_path+" -b "+KEIL_PROJECT_PATH+" -j0")
os.system(keil_path+" -d "+KEIL_PROJECT_PATH+" -j0")

# Espera a que el archivo de finalizacion aparezca lo que denota el fin de ensamblador
while (not (os.path.exists(TEMP_PATH+"done.hex"))):
    time.sleep(1)

#Se transforman los archivos hexadecimales a binarios
for name in filter_names:
    hex2bin(TEMP_PATH+name+"_k1.hex", TEMP_PATH+name+"_k1.bin")
    hex2bin(TEMP_PATH+name+"_k2.hex", TEMP_PATH+name+"_k2.bin")
new_img_h = image_height-2
new_img_w = image_width-2


## Abre los archivos bianrios que fueron creados
for name in filter_names:
    with open(TEMP_PATH+name+"_k1.bin", mode='rb') as f1:
        k1_filter = np.fromfile(
            f1, dtype=np.int16, count=new_img_w*new_img_h).reshape(new_img_h, new_img_w)
        cv2.imwrite(RESULT_PATH+name+"_k1.png", k1_filter)
        images_k1.append(k1_filter)

    with open(TEMP_PATH+name+"_k2.bin", mode='rb') as f2:
        k2_filter = np.fromfile(
            f2, dtype=np.int16, count=new_img_w*new_img_h).reshape(new_img_h, new_img_w)
        cv2.imwrite(RESULT_PATH+name+"_k2.png", k2_filter)
        images_k2.append(k2_filter)

#Se eliminan las columnas de ceros agregadas del alto y ancho
resized_image = []
resized_image.append(red_channel[:-2, :-2])
resized_image.append(green_channel[:-2, :-2])
resized_image.append(blue_channel[:-2, :-2])

## Se realiza la resta de las capas con la imagen original
## ESTO  SOLO ES REQUERIDO SI SE UTILIZA "BOTTOM SOBEL" Y "OUTLINE"

"""
substracted_filter_k1 = []
substracted_filter_k2 = []

for image_filter, r_image in zip(images_k1, resized_image):
    substracted_filter_k1.append(np.subtract(r_image, image_filter))

for image_filter, r_image in zip(images_k2, resized_image):
    substracted_filter_k2.append(np.subtract(r_image, image_filter))
newimage3 = cv2.merge(
    (substracted_filter_k2[2], substracted_filter_k2[1], substracted_filter_k2[0]))
    """
## Creacion de las imagenes resultantes
Image_sharpening = cv2.merge(
    (images_k1[2], images_k1[1], images_k1[0]))

Image_oversharpening = cv2.merge(
    (images_k2[2], images_k2[1], images_k2[0]))

## Se crean los archivos de las imagenes resultantes
cv2.imwrite(RESULT_PATH+"Image_sharpening.png", Image_sharpening)
cv2.imwrite(RESULT_PATH+"Image_oversharpening.png", Image_oversharpening)
print( "Las dimensiones del las imagnes es :" + str(Image_oversharpening.shape))

## Descomentar para eliminar los archivos temporales
""" files = glob.glob(TEMP_PATH+"/*")
for f in files:
    os.remove(f)
files = glob.glob(INPUT_PATH+"/*")
for f in files:
    os.remove(f) """
 ## Se despliegan las nuevas imagenes en la pantalla
easygui.msgbox('Las ventanas de las imagenes pueden ser redimensionadas \n Presione una tecla para cerrar las imagenes \n Las imagenes resultantes encuentran en la carpeta data/result', 'Información')
Image_sharpening = cv2.imread(RESULT_PATH+"Image_sharpening.png")
Image_oversharpening = cv2.imread(RESULT_PATH+"Image_oversharpening.png")
Image_sharpening_R = cv2.imread(RESULT_PATH+"red_k1.png")
Image_oversharpening_R = cv2.imread(RESULT_PATH+"red_k2.png")




cv2.namedWindow("Imagen_Original",cv2.WINDOW_NORMAL)
cv2.namedWindow("Capa R Original",cv2.WINDOW_NORMAL)
cv2.namedWindow("Capa R Sharpening",cv2.WINDOW_NORMAL)
cv2.namedWindow("Capa R OverSharpening",cv2.WINDOW_NORMAL)
cv2.namedWindow("Imagen con OverSharpening",cv2.WINDOW_NORMAL )
cv2.namedWindow("Imagen con Sharpening",cv2.WINDOW_NORMAL)

cv2.moveWindow("Imagen_Original",0,300)
cv2.moveWindow("Capa R Original",300,0)
cv2.moveWindow("Capa R Sharpening",600,300)
cv2.moveWindow("Capa R OverSharpening",900,500)
cv2.moveWindow("Imagen con OverSharpening",1200,0)
cv2.moveWindow("Imagen con Sharpening",1200,300)

cv2.imshow("Imagen_Original", image)
cv2.imshow("Capa R Original", red_channel)
cv2.imshow("Capa R Sharpening", Image_sharpening_R)
cv2.imshow("Capa R OverSharpening", Image_oversharpening_R)
cv2.imshow("Imagen con OverSharpening", Image_oversharpening)
cv2.imshow("Imagen con Sharpening", Image_sharpening)

cv2.resizeWindow("Imagen_Original",int(image_width),int(image_height))
cv2.resizeWindow("Capa R Original",int(image_width),int(image_height))
cv2.resizeWindow("Capa R Sharpening",int(image_width),int(image_height))
cv2.resizeWindow("Capa R OverSharpening",int(image_width),int(image_height))
cv2.resizeWindow("Imagen con OverSharpening",int(image_width),int(image_height))
cv2.resizeWindow("Imagen con Sharpening",int(image_width),int(image_height))


cv2.waitKey(0)
cv2.destroyAllWindows()

