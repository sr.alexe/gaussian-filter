# EagleSharpView!

In the EagleSharpView.py file there is an attempt to implement the kernel convolution for Image Sharpening.  This method consist in pre-processing in Python 3.7 using OpenCV an implementation of the convolution algorithm in ARMv7 assembly and a post-processing using, again, Python 3.7 with IntelHex and OpenCV. This software is created for Windows platform but can be easily migrated to Linux

The motivation is to create a **native** program that can bring this intensive functionality with less overhead.

---

# Files
|FILE|LANGUAGE|DESCRIPTION|
|-|-|-|
|EagleSharpView.py|`Python`|This is a script that does the pre-processing and post-processing. It start the Assembly compilation and runs the Keil uVision Debug console.|
|Image-sharpening.s|`ARM Assembly`|File in assembly language. Includes implementation of the Image Sharpening algorithm |
|commands.ini|`Configuration File`|File to configure the debug execution of the file compiled|
|*.hex|`IntelHex `|Files with the results of the convolution in HEX386 format|
|*.bin|`Binary`|Files with the interpretation of the HEX files in raw binary format|

---

## Requisites and Dependencies

To run EagleSharpView the following software requisites must be met. 

|PROGRAM|VERSION|
|-|-|
|`Keil μVision`|*5.x*|
|`Pyhton`|*3.7.x or newer*|

For Python 3.7.x the following libraries must be installed.

|LIBRARY| VERSION|
|-|-|
|`Numpy`|*1.18.1^*|
|`EasyGUI`|*0.98.1^*|
|`Intelhex`|*2.2.1^*|
|`OpenCV-Python`|*4.2.0.34^*|

### To avoid the bad configured environments and unused dependencies is recommended to install Anaconda and create a new enviroment firts this can be achived with the following command

```bash
conda create --name *myenv*
```

Then enable the new environment

```bash
conda activate *myenv*
```
---

## How to Run
1. Install Keil uVision v5 (MDK-Arm) https://armkeil.blob.core.windows.net/eval/MDK530.EXE
2. Install the Keil Legacy Package https://armkeil.blob.core.windows.net/legacy/MDK79525.EXE
3. Install Anaconda
4. Create a new environment in anaconda
5. Use the python instance tu run the script EagleSharpView.py

```bash
>python EagleSharpView.py
```
6. A dialog will open that will ask for the Keil uVision installation folder if it can not be detected automatically.
7. Another dialog will appear and the image to process must be selected.
8. The height and Width will be asked to the user
9. Due to the heavy memory usage and the size of the image the process of convolution could take from 1 to 5 minutes.
10. Five Windows should open with the resulting images. 
11. Resulting files are located in the data/result directory.

---
## Built With

* [Anaconda](https://www.anaconda.com/) - The enviroments manager
* [Python](https://www.python.org/) - Programming language
* [EasyGUI](https://pythonhosted.org/easygui/index.html) - Used to the dialogs
* [Numpy](https://numpy.org/) - Used to run complex arrays
* [Keil uVision](http://www2.keil.com/mdk5/uvision/) - Used to compile and run ARM assembly
* [Visual Studio Code](https://code.visualstudio.com/) - IDE used for the development
---
## Authors

* **Alex Saenz Rojas** - *Initial work* - [sr.alexe](https://gitlab.com/sr.alexe/)

---

## License

This project is licensed under the GPL License - see the [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) file for details

This program will be publicly available the 1st of July of 2020.

---

## Acknowledgments

* Thanks to my mom and GF fo all the coffee and patience

---
