		GLOBAL GAUSIAN_FILTER
		AREA FILTER, CODE, READONLY
		ENTRY
GAUSIAN_FILTER
		MOV R0,#0
GREEN
		INCBIN     ..\data\input\green.bin
RED
		INCBIN     ..\data\input\red.bin
BLUE
		INCBIN     ..\data\input\blue.bin
KERNEL1
		INCBIN     ..\data\input\kernel1.bin
KERNEL2
		INCBIN     ..\data\input\kernel2.bin
DIMENSIONS
		INCBIN     ..\data\input\dimensions.bin
Initialize
		LDR R10, ImageAddress
		LDR R11, Green
		STR R11, [R10]		
		LDR R10, KernelAddress
		LDR R11, Kernel1
		STR R11, [R10]
GetAddresses
		MOV R0,#-1
		MOV R1,#-1
		MOV R2,#0x1100000; Where to store
		MOV R3,#0
		MOV R4,#1
		MOV R5,#1		
LoadActivePixel
		ADD R6,R0,R4; Suma de posiciones en X (imagen)
		MOV R11,#2; half word
		MUL R12, R6,R11; Posicion real en X
		MOV R6,R12
		
		LDRSH R10,DIMENSIONS; get Width
		
		ADD R7, R1,R5; Suma de posiciones en Y (imagen)		
		MUL R12,R10,R7; Posicion relativa en Y (imagen)
		MOV R7,R12

		MUL R12,R7,R11; half word
		MOV R7,R12
		ADD R6,R6,R7; Pixel activo X+Y
		
		LDR	R11,ImageAddress
		LDR R11,[R11];address of active color
		;LDR R11, [R11];offset of active color
		
		ADD R6,R11; Direccion Pixel activo 
		LDRSH R6,[R6]
LoadKernel

		ADD R9,R0,#1 ;Posicion en X (Kernel) ajuste numeros negativos
		MOV R11,#2 ;halfword
		MUL R12,R9,R11 ; Posicion real en X
		MOV R9,R12
		
		MOV R10, #6
		ADD R8,R1,#1 ;Posicion  en Y (Kernel) (Suma de Ys) Ajuste numeros negativos
		MUL R12,R10,R8 ;Posicion Relativa en Y (Ys x Ancho)
		MOV R8, R12
		;MUL R12,R8,R11 ;Posicion Real en Y ) (Ys x Bytes)
		MOV R8, R12
		ADD R8,R9,R8 ; Kernel Activo Relativo
		
		LDR R11, KernelAddress
		LDR R11,[R11]
		;LDR R11,[R11]
		
		ADD R8,R8,R11; Direccion kernel activo 
		LDRSH	R8,[R8]

ComputePixel
		MUL R9,R8,R6
		ADD R3,R9,R3
		B AdvanceX
StoreNewPixel
		STRH R3,[R2] ;
		ADD R2, #2; Next
		MOV R3, #0
		B AdvanceXPointer		
AdvanceX
		ADD R0,#1
		CMP R0,#2
		BGE AdvanceY
		B LoadActivePixel
AdvanceY
		MOV R0, #-1
		ADD R1,#1
		CMP R1,#2
		BGE StoreNewPixel
		;BX R14
		B LoadActivePixel
AdvanceXPointer
		MOV R1, #-1
		ADD R4, #1
		
		LDR R12, DimensionsAddress; get Width
		LDRH R12, [R12]; get Width


		SUB R12,R12,#1; menos la ultima  columna
		CMP R4,R12
		BGE AdvanceYPointer
		;BX R14
		B LoadActivePixel
AdvanceYPointer
		MOV R4,#1
		ADD R5, #1
		LDR R12 , DimensionsAddress
		ADD R12,#2
		LDRH R12, [R12]; get Height
		SUB R12,R12,#1;evitar la ultima fila
		CMP R5,R12
		BGE NextColor
		;BX R14
		B LoadActivePixel
NextColor 
		LDR R0,State
		LDR R0, [R0]
		CMP R0,#0
		BEQ Green_K1
		CMP R0,#1
		BEQ Red_K1
		CMP R0,#2
		BEQ Blue_K1
		CMP R0,#3
		BEQ Green_K2
		CMP R0,#4
		BEQ Red_K2
		CMP R0,#5
		BEQ Blue_K2
ENDOP 	
		MOV R0,#0
		MOV R1,#1
		B ENDOP
Green_K1
		ADD R0,R0,#1
		LDR R1,State
		STR R0,[R1]		
		LDR R10, ImageAddress
		LDR R11, Red 
		STR R11, [R10]
		B GetAddresses
Red_K1
		ADD R0,R0,#1
		LDR R1,State
		STR R0,[R1]		
		LDR R10, ImageAddress
		LDR R11, Blue 
		STR R11, [R10]			
		B GetAddresses
Blue_K1
		ADD R0,R0,#1
		LDR R1,State
		STR R0,[R1]		
		LDR R10, ImageAddress
		LDR R11, Green 
		STR R11, [R10]
		LDR R10, KernelAddress
		LDR R11, Kernel2
		STR R11, [R10]			
		B GetAddresses

Green_K2
		ADD R0,R0,#1
		LDR R1,State
		STR R0,[R1]		
		LDR R10, ImageAddress
		LDR R11, Red 
		STR R11, [R10]			
		B GetAddresses
Red_K2
		ADD R0,R0,#1
		LDR R1,State
		STR R0,[R1]		
		LDR R10, ImageAddress
		LDR R11, Blue 
		STR R11, [R10]			
		B GetAddresses
Blue_K2
		ADD R0,R0,#1
		LDR R1,State
		STR R0,[R1]		
		B ENDOP


ImageAddress		DCD   0x1000000
Green DCD GREEN
Red DCD RED
Blue DCD BLUE	
KernelAddress		DCD   0x1000100	
Kernel1		DCD   KERNEL1
Kernel2		DCD   KERNEL2
DimensionsAddress DCD DIMENSIONS
State DCD 0x1000200
KernelSize	DCD		6
	
		

		END
			;SAVE ..\data\green_result.hex  0x1000000,0x1194e88
			;MAP 0x100000,0x194e88 READ WRITE

